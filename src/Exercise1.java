import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class Exercise1 {
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JButton gyozaButton;
    private JButton sobaButton;
    private JButton karaageButton;
    private JLabel orderedItemLabel;
    private JButton checkOutButton;
    private JLabel totalLabel;
    private JTextPane textPane1;


    //class
    public Exercise1() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura",350);
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen",750);

            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon",600);
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza",400);
            }
        });
        sobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Soba",650);
            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage",440);
            }
        });

        textPane1.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
            }
        });
        totalLabel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
            }
        });


        //checkOutボタン
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int comfirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Checkout Confimation",
                        JOptionPane.YES_NO_OPTION
                );

                if(comfirmation==0){
                    //学割
                    comfirmation = JOptionPane.showConfirmDialog(
                            null,
                            "Are you a student now?",
                            "Student Discount 20%",
                            JOptionPane.YES_NO_OPTION
                    );

                    if(comfirmation==0){
                        totalPrice = (int)(totalPrice*0.8);
                    }

                    int tax = (int)(totalPrice*1.1);

                    JOptionPane.showMessageDialog(
                            null,
                            "Thank you. The total price is "+totalPrice+" (tax included "+tax+") yen."
                    );

                    totalPrice=0;
                    totalLabel.setText("Total "+totalPrice+" yen");

                    String currentText = null;
                    textPane1.setText(currentText);
                }

            }
        });
    }

    //main関数
    public static void main(String[] args) {
        JFrame frame = new JFrame("Exercise1");
        frame.setContentPane(new Exercise1().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


    int totalPrice=0;
    //order関数定義
    void order(String foodName, int foodPrice){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order "+foodName+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if(confirmation == 0){

            totalPrice += foodPrice;

            String currentText = textPane1.getText();
            textPane1.setText(currentText + foodName + " " + foodPrice + "yen\n");

            JOptionPane.showMessageDialog(null, "Order for "+foodName+" It will served as soon as possible.");

            totalLabel.setText("Total "+totalPrice + "yen");
        }
    }
}


